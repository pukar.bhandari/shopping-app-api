const express = require('express');

const { addOrder, getOrderofUser } = require('../controllers/orders');

const router = express.Router();

const { protect } = require('../middleware/auth');

router.route('/').post(protect, addOrder);

router.route('/:userId').get(protect, getOrderofUser);

module.exports = router;
