const express = require('express');

const {
    getProducts,
    getProduct,
    addProduct,
    updateProduct,
    deleteProduct,
    productPhotoUpload,
    getProductaddedbyUser
} = require('../controllers/products');

const router = express.Router();

const { protect } = require('../middleware/auth');

router.route('/').get(getProducts).post(protect, addProduct);

router.route('/:id')
    .get(getProduct)
    .put(protect, updateProduct)
    .delete(protect, deleteProduct);

router.route('/:id/photo').put(protect, productPhotoUpload);

router.route('/user/:userId').get(protect, getProductaddedbyUser);

module.exports = router;
