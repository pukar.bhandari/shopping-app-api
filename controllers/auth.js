const admin = require('firebase-admin');
const db = admin.firestore();
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const sendEmail = require('../utils/sendEmail');

// @desc        Register user
// @route       POST /api/v1/auth/register
// @access      Public
exports.register = asyncHandler(async (req, res, next) => {
    const { name, email, username, password, role } = req.body;

    //  Encrypt password using bcrypt
    const hashedPassword = await encryptPassword(password);
    // Create user
    const user = await db.collection('users').add({
        name,
        email,
        username,
        role: role ? role : 'user',
        password: hashedPassword
    });

    sendTokenResponse(user,200, res);

});

// @desc        Login user
// @route       POST /api/v1/auth/login
// @access      Public
exports.login = asyncHandler(async (req, res, next) => {
    const { username, password } = req.body;

    // Validate username and password
    if(!username || !password) {
        return next(new ErrorResponse('Please provide username and password', 400));
    }

    // Check for user
    const user = await db.collection('users').where('username', '==', username).limit(1).get();

    if(user.empty) {
        return next(new ErrorResponse('Invalid credentials', 401));
    }

    let userPassword = '';
    let userData = {};
    user.forEach(doc => {
        userPassword = doc.data().password;
        userData = {id: doc.id, ...doc.data()};
    });

    // Check if password matches
    const isMatch = await matchPassword(password, userPassword);

    if(!isMatch) {
        return next(new ErrorResponse('Invalid credentials', 401));
    }

    sendTokenResponse(userData, 200, res);
});

// @desc        Log user out / clear cookie
// @route       GET /api/v1/auth/logout
// @access      Private
exports.logout = asyncHandler(async (req, res, next) => {
    res.cookie('token','none', {
        expires: new Date(Date.now() + 10 * 1000),
        httpOnly: true
    });

    return res.status(200).json({
        success: true,
        data: {}
    });
});

// @desc        Get current logged in user
// @route       GET /api/v1/auth/me
// @access      Private
exports.getMe = asyncHandler(async (req, res, next) => {
    const user = req.user;
    return res.status(200).json({
        success: true,
        data: user
    });
});

// @desc        Update user details
// @route       PUT /api/v1/auth/updatedetails
// @access      Private
exports.updateDetails = asyncHandler(async (req, res, next) => {

    const userRef = db.collection('users').doc(req.user.id);

    await userRef.update(req.body);
    const user = await userRef.get();
    const fieldsToUpdate = {
        name: user.data().name,
        email: user.data().email,
        username: user.data().username
    }

    return res.status(200)
        .json({
            success: true,
            data: fieldsToUpdate
        });

});

// @desc        Update password
// @route       PUT /api/v1/auth/updatepassword
// @access      Private
exports.updatePassword = asyncHandler(async (req, res, next) => {
    const userRef = db.collection('users').doc(req.user.id);
    const user = await userRef.get();

    // Check current password
    if(!(await matchPassword(req.body.currentPassword, user.data().password))) {
        return next(new ErrorResponse('Password is incorrect', 401));
    }

    //  Encrypt password using bcrypt
    const password = await encryptPassword(req.body.newPassword);

    await userRef.update({
        password
    });

    sendTokenResponse(user, 200, res);
});

// @desc        Forgot password
// @route       POST /api/v1/auth/forgotpassword
// @access      Public
exports.forgotPassword = asyncHandler(async (req, res, next) => {
    const userRef = db.collection('users').where('email','==', req.body.email);
    const user = await userRef.get();

    if(user.empty) {
        return next(new ErrorResponse('There is no user with that email', 404));
    }

    //Get userId
    let userId = '';
    let userEmail = '';
    user.forEach(appUser => {
        userId = appUser.id;
        userEmail = appUser.data().email
    });

    // Get reset token
    const resetToken = await getResetPasswordToken(userId);

    // Create reset url
    const resetUrl = `${req.protocol}://${req.get('host')}/api/v1/auth/resetpassword/${resetToken}`;

    const message = `You are receiving this email because you (or someone else) has requested the reset of a password.
    Please make a PUT request to: \n\n ${resetUrl}`;

    try {
        await sendEmail({
            email: userEmail,
            subject: 'Password reset token',
            message
        });
        return res.status(200).json({ success: true, data: 'Email sent' });
    } catch (err) {
        console.log(err);
        const FieldValue = admin.firestore.FieldValue;
        const userRef = db.collection('users').doc(userId);
        await userRef.update({
            resetPasswordToken: FieldValue.delete(),
            resetPasswordExpire: FieldValue.delete()
        })
        return next(new ErrorResponse('Email could not be sent', 500));
    }
});

// @desc        Reset password
// @route       PUT /api/v1/auth/resetpassword/:resetoken
// @access      Public
exports.resetPassword = asyncHandler(async (req, res, next) => {
    // Get hashed token
    const resetPasswordToken = crypto.createHash('sha256').update(req.params.resettoken).digest('hex');

    const userRef = db.collection('users')
        .where('resetPasswordToken','==', resetPasswordToken)
        .where('resetPasswordExpire','>', Date.now());
    const user = await userRef.get();

    if(user.empty) {
        return next(new ErrorResponse('Invalid token', 400));
    }

    let userId = '';
    user.forEach(appUser => {
        userId = appUser.id
    });
    //  Encrypt password using bcrypt
    const password = await encryptPassword(req.body.password);

    const appUserRef = db.collection('users').doc(userId);
    const appUser = await appUserRef.get();
    const FieldValue = admin.firestore.FieldValue;

    await appUserRef.update({
        password,
        resetPasswordToken: FieldValue.delete(),
        resetPasswordExpire: FieldValue.delete()
    })

    sendTokenResponse(appUser, 200, res);
});

// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode, res) => {
    // Create token
    const token = getSignedJwtToken(user.id);

    const options = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000),
        httpOnly: true
    };

    if(process.env.NODE_ENV === "production") {
        options.secure = true;
    }

    res.status(statusCode)
        .cookie('token', token, options)
        .json({
            success: true,
            token
        });
};

//  Encrypt password using bcrypt
const encryptPassword = async function(password) {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
}

// Sign JWT and return
const getSignedJwtToken = function(userId) {
    return jwt.sign({ id: userId}, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRE
    });
};

// Match user entered password to hashed password in database
const matchPassword = async function (enteredPassword, userPassword) {
    return await bcrypt.compare(enteredPassword, userPassword);
}

// Generate and hash password token
const getResetPasswordToken = async function(userId) {
    // Generate token
    const resetToken = crypto.randomBytes(20).toString('hex');

    // hash token and set to resetPasswordToken field
    const resetPasswordToken = crypto
        .createHash('sha256')
        .update(resetToken)
        .digest('hex');

    // Set expire
    const resetPasswordExpire = Date.now() + 10 * 60 * 1000;

    const userRef = db.collection('users').doc(userId);

    await userRef.update({
        resetPasswordToken,
        resetPasswordExpire
    })

    return resetToken;
}
