const admin = require('firebase-admin');
const db = admin.firestore();
const bcrypt = require('bcryptjs');
const asyncHandler = require('../middleware/async');

// @desc        Get all users
// @route       GET /api/v1/users
// @access      Private/Admin
exports.getUsers = asyncHandler(async (req, res, next) => {
    let users = [];
    const usersRef = db.collection('users');
    const snapshot = await usersRef.get();
    snapshot.forEach(user => {
        users.push({ id: user.id, ...user.data()});
    });
    res.status(200).json({
        success: true,
        data: users
    });
});

// @desc        Get single user
// @route       GET /api/v1/users/:id
// @access      Private/Admin
exports.getUser = asyncHandler(async (req, res, next) => {
    const userRef = db.collection('users').doc(req.params.id);
    const user = await userRef.get();
    return res.status(200).json({
        success: true,
        data: user.data()
    });
});

// @desc        Create user
// @route       POST /api/v1/users
// @access      Private/Admin
exports.createUser = asyncHandler(async (req, res, next) => {
    //  Encrypt password using bcrypt
    const salt = await bcrypt.genSalt(10);
    req.body.password = await bcrypt.hash(req.body.password, salt);

    req.body.role = req.body.role ? req.body.role : 'user';

    await db.collection('users').add(req.body);

    return res.status(201).json({
        success: true
    });
});

// @desc        Update user
// @route       PUT /api/v1/users/:id
// @access      Private/Admin
exports.updateUser = asyncHandler(async (req, res, next) => {
    const userRef = db.collection('users').doc(req.params.id);
    await userRef.update(req.body);
    const user = await userRef.get();

    return res.status(200).json({
        success: true,
        data: user.data()
    });
});

// @desc        Delete user
// @route       DELETE /api/v1/users/:id
// @access      Private/Admin
exports.deleteUser = asyncHandler(async (req, res, next) => {
    await db.collection('users').doc(req.params.id).delete();

    return res.status(200).json({
        success: true,
        data: {}
    });
});
