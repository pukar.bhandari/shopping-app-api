const admin = require('firebase-admin');
const db = admin.firestore();
const path = require('path');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');

// @desc        Get all products
// @route       GET /api/v1/products
// @access      Public
exports.getProducts = asyncHandler(async (req, res, next) => {
    let products = [];
    const productsRef = db.collection('products');
    const snapshot = await productsRef.get();
    snapshot.forEach(product => {
        products.push({id: product.id, ...product.data()});
    });
    return res.status(200).json({
        success: true,
        data: products
    });
});

// @desc        Get product
// @route       GET /api/v1/products/:id
// @access      Public
exports.getProduct = asyncHandler(async (req, res, next) => {
    const productRef = db.collection('products').doc(req.params.id);
    const doc = await productRef.get();
    if (!doc.exists) {
        return next(new ErrorResponse(`Product not found with id of ${req.params.id}`, 404));
    } else {
        return res.status(200).json({
            success: true,
            data: doc.data()
        });
    }
});

// @desc        Get product added by user
// @route       GET /api/v1/products/user/:userId
// @access      Private
exports.getProductaddedbyUser = asyncHandler(async (req, res, next) => {

    const products = await db.collection('products').where('user', '==', req.params.userId).get();

    let productData = [];
    products.forEach(doc => {
        productData.push({id: doc.id, ...doc.data()});
    });

    return res.status(200).json({
        success: true,
        data: productData
    });
});

// @desc        Add new product
// @route       POST /api/v1/products
// @access      Private
exports.addProduct = asyncHandler(async (req, res, next) => {

    // Add user to req.body
    req.body.user = req.user.id;

    // Get the `FieldValue` object
    const FieldValue = admin.firestore.FieldValue;
    req.body.timestamp = FieldValue.serverTimestamp();

    const product = await db.collection('products').add(req.body);

    return res.status(201).json({
        success: true,
        id: product.id
    });
});

// @desc        Update product
// @route       PUT /api/v1/products/:id
// @access      Private
exports.updateProduct = asyncHandler(async (req, res, next) => {
    const productRef = db.collection('products').doc(req.params.id);
    let doc = await productRef.get();

    if (!doc.exists) {
        return next(new ErrorResponse(`Product not found with id of ${req.params.id}`, 404));
    }

    // Make sure user is product owner
    if (doc.data().user !== req.user.id && req.user.role !== "admin") {
        return next(new ErrorResponse(`User ${req.params.id} is not authorized to update this product`, 401));
    }
    // Get the `FieldValue` object
    const FieldValue = admin.firestore.FieldValue;
    req.body.timestamp = FieldValue.serverTimestamp();

    doc = await productRef.update(req.body);
    doc = await productRef.get();
    return res.status(200).json({success: true, data: doc.data()});

});

// @desc        Delete product
// @route       DELETE /api/v1/products/:id
// @access      Private
exports.deleteProduct = asyncHandler(async (req, res, next) => {
    const productRef = db.collection('products').doc(req.params.id);
    let doc = await productRef.get();

    if (!doc.exists) {
        return next(new ErrorResponse(`Product not found with id of ${req.params.id}`, 404));
    }

    // Make sure user is product owner
    if(doc.data().user !== req.user.id && req.user.role !== "admin") {
        return next(new ErrorResponse(`User ${req.params.id} is not authorized to update this product`, 401));
    }

    doc = await productRef.delete();
    return res.status(200).json({success: true, data: {}});

});

// @desc        Upload photo for product
// @route       PUT /api/v1/products/:id/photo
// @access      Private
exports.productPhotoUpload = asyncHandler(async (req, res, next) => {
    const productRef = db.collection('products').doc(req.params.id);
    let doc = await productRef.get();

    if (!doc.exists) {
        return next(new ErrorResponse(`Product not found with id of ${req.params.id}`, 404));
    }

    // Make sure user is product owner
    if(doc.data().user !== req.user.id && req.user.role !== "admin") {
        return next(new ErrorResponse(`User ${req.params.id} is not authorized to update this product`, 401));
    }

    if (!req.files) {
        return next(new ErrorResponse('Please upload a file', 400));
    }

    const file = req.files.file;

    // Make sure the image is a photo
    if (!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse('Please upload an image file', 400));
    }

    // Check filesize
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(new ErrorResponse(`Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`, 400));
    }

    // Create custom filename
    file.name = `photo_${req.params.id}${path.parse(file.name).ext}`;

    file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if (err) {
            console.error(err);
            return next(new ErrorResponse(`Problem with file upload`, 500));
        }

        await productRef.update({photo: file.name});

        return res.status(200).json({success: true, data: file.name});
    })

});
