const admin = require('firebase-admin');
const db = admin.firestore();
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');

// @desc        Add new order
// @route       POST /api/v1/orders
// @access      Private
exports.addOrder = asyncHandler(async (req, res, next) => {

    // Add user to req.body
    req.body.user = req.user.id;

    // Get the `FieldValue` object
    const FieldValue = admin.firestore.FieldValue;
    req.body.timestamp = FieldValue.serverTimestamp();

    const order = await db.collection('orders').add(req.body);

    return res.status(201).json({
        success: true,
        id: order.id
    });
});

// @desc        Get order of user
// @route       GET /api/v1/orders/:userId
// @access      Private
exports.getOrderofUser = asyncHandler(async (req, res, next) => {

    const order = await db.collection('orders').where('user', '==', req.params.userId).get();

    let orderData = [];
    order.forEach(doc => {
        orderData.push({id: doc.id, ...doc.data()});
    });

    return res.status(200).json({
        success: true,
        data: orderData
    });
});

